package oster;

import javax.xml.soap.SOAPMessage;

import org.tempuri.GetAutoamtBasic;
import org.tempuri.GetAutoamtBasicResponse;
import org.tempuri.GetItemAmt;
import org.tempuri.GetItemAmtResponse;
import org.tempuri.GetPatPayStatus;
import org.tempuri.GetPatPayStatusResponse;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TaipeiMediaSample {

//	public final static String SERVER_URL = "http://203.71.89.114:9999/AutoPayService/Service.asmx";
	public final static String SERVER_URL = "http://203.71.89.114:9999/AutoPayService_TEST/Service.asmx";
	
	public static void main(String[] args) throws Exception {
		inquiryFee();
//		inquiryFeeDetail();
	}

	static GetAutoamtBasicResponse inquiryFee() throws Exception {
		GetAutoamtBasic requestBody = new GetAutoamtBasic();
		requestBody.setARGFEENO("O50467687");
		
//		GetAutoamtBasicResponse response = new TaipeiMediaSoapCallerService().callInquiryFee(
//				SERVER_URL, 
//				"http://tempuri.org/Get_autoamt_basic", 
//				requestBody);
//		response.getGetAutoamtBasicResult().getContent();
//		return response;
		SOAPMessage soapResponse = new TaipeiMediaSoapCallerService().callInquiryFeeWithSoap(
				SERVER_URL, 
				"http://tempuri.org/Get_autoamt_basic_APP", 
				requestBody);
		
		soapResponse.writeTo(System.out); System.out.println();
		
		Node autoSum = soapResponse.getSOAPBody().getFirstChild().getFirstChild().getFirstChild();
		
		NodeList autoSumChild = autoSum.getChildNodes();
		for (int i = 0 ; i < autoSumChild.getLength(); i++) {
			Node childNode = autoSumChild.item(i);
			System.out.println("autoSum.children at " + i + " is " + childNode.getNodeName() + " content " + childNode.getTextContent());
		}
		
		return null;
		
	}
	
	static GetItemAmtResponse inquiryFeeDetail() throws Exception {
		GetItemAmt requestBody = new GetItemAmt();
		requestBody.setARGFEENO("O50467547");
		requestBody.setARGPAYTYPE("3");//信用卡
		requestBody.setARGBANKNO("");
		requestBody.setARGBANKTYPE("");
		requestBody.setARGFEEDT("");
		requestBody.setARGINSFLAG("");
		requestBody.setARGMACHINENO("");
		requestBody.setARGREALAMT("");
		
//		GetAutoamtBasicResponse response = new TaipeiMediaSoapCallerService().callInquiryFee(
//				SERVER_URL, 
//				"http://tempuri.org/Get_autoamt_basic", 
//				requestBody);
//		response.getGetAutoamtBasicResult().getContent();
//		return response;
		SOAPMessage soapResponse = new TaipeiMediaSoapCallerService().callInquiryFeeDetailWithSoap(
				SERVER_URL, 
				"http://tempuri.org/Get_item_amt", 
				requestBody);
		
		soapResponse.writeTo(System.out); System.out.println();
		
//		Node autoSum = soapResponse.getSOAPBody().getFirstChild().getFirstChild().getFirstChild();
//		
//		NodeList autoSumChild = autoSum.getChildNodes();
//		for (int i = 0 ; i < autoSumChild.getLength(); i++) {
//			Node childNode = autoSumChild.item(i);
//			System.out.println("autoSum.children at " + i + " is " + childNode.getNodeName() + " content " + childNode.getTextContent());
//		}
		
		return null;
		
	}
	
	static GetPatPayStatusResponse inquiryFeeStatus() throws Exception {
		GetPatPayStatus requestBody = new GetPatPayStatus();
		requestBody.setARGFEENO("O50467547");
		
		SOAPMessage soapResponse = new TaipeiMediaSoapCallerService().callInquiryFeeStatusWithSoap(
				SERVER_URL, 
				"http://tempuri.org/Get_PatPay_Status", 
				requestBody);
		
		soapResponse.writeTo(System.out); System.out.println();
		
		Node autoSum = soapResponse.getSOAPBody().getFirstChild().getFirstChild().getFirstChild();
//		
		NodeList autoSumChild = autoSum.getChildNodes();
		for (int i = 0 ; i < autoSumChild.getLength(); i++) {
			Node childNode = autoSumChild.item(i);
			System.out.println("autoSum.children at " + i + " is " + childNode.getNodeName() + " content " + childNode.getTextContent());
		}
		
		return null;
		
	}
	
}
