package oster;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;


public class SoapUtil {
	
	public static <T> SOAPMessage createSOAPRequest(String serverURI, String action, T requestBody) throws Exception {
        //
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();

        // marshall Body Object to W3c document, document to SOAP Body
        Document bodyDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();

        Marshaller marshaller = JAXBContext.newInstance(requestBody.getClass()).createMarshaller();
        marshaller.marshal(requestBody, bodyDocument);

        envelope.getBody().addDocument(bodyDocument);

        // Set Protocal Header
        if (action != null) {
	        MimeHeaders headers = soapMessage.getMimeHeaders();
	        headers.addHeader("SOAPAction", action);
	        soapMessage.saveChanges();
        }

        return soapMessage;
    }

	public static <T> T transferToResult(Class<T> targetClass, SOAPMessage soapResponse) throws JAXBException, XMLStreamException, IOException, SOAPException {
        soapResponse.writeTo(System.out);
        
        // Determine the Tag Name
        String tagName = targetClass.getSimpleName();
        XmlRootElement rootElementAnnotation = targetClass.getAnnotation(XmlRootElement.class);
        if (rootElementAnnotation.name() != null)
        	tagName = rootElementAnnotation.name();
        
        //Use XML Reader to find the Root Element
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        soapResponse.writeTo(bos);
        XMLInputFactory xif = XMLInputFactory.newFactory();
        XMLStreamReader xsr = xif.createXMLStreamReader(new StreamSource(new ByteArrayInputStream(bos.toByteArray())));

        xsr.nextTag();
        while (!xsr.getLocalName().equals(tagName)) {
            xsr.nextTag();
        }

        JAXBContext jc = JAXBContext.newInstance(targetClass);
        Unmarshaller unmarshaller = jc.createUnmarshaller();
        JAXBElement<T> jb = unmarshaller.unmarshal(xsr, targetClass);
        xsr.close();

        return jb.getValue();

    }
	
}
