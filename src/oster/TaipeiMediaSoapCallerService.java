package oster;

import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPMessage;

import org.tempuri.GetAutoamtBasic;
import org.tempuri.GetAutoamtBasicResponse;
import org.tempuri.GetItemAmt;
import org.tempuri.GetPatPayStatus;

public class TaipeiMediaSoapCallerService {

	public GetAutoamtBasicResponse callInquiryFee(String url, String nsUri, GetAutoamtBasic requestBody) throws Exception {
		// Create SOAP Connection
        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection soapConnection = soapConnectionFactory.createConnection();

        // Send SOAP Message to SOAP Server
        SOAPMessage soapRequest = SoapUtil.createSOAPRequest(url, nsUri, requestBody);
        soapRequest.writeTo(System.out);
        System.out.println();
        
        SOAPMessage soapResponse = soapConnection.call(
        		soapRequest, 
        		url);
        
        
        System.out.println("Transfer to Object - Call transfer()");
        if (soapResponse.getSOAPBody().hasFault()) {
        	System.out.println("Has fault.....");
        	soapResponse.writeTo(System.out);
        	return null;
        }
        
        GetAutoamtBasicResponse responseObject = SoapUtil.transferToResult(GetAutoamtBasicResponse.class, soapResponse);

        soapConnection.close();
        return responseObject;
	}
	
	public SOAPMessage callInquiryFeeWithSoap(String url, String nsUri, GetAutoamtBasic requestBody) throws Exception {
		// Create SOAP Connection
        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection soapConnection = soapConnectionFactory.createConnection();

        // Send SOAP Message to SOAP Server
        SOAPMessage soapRequest = SoapUtil.createSOAPRequest(url, nsUri, requestBody);
        soapRequest.writeTo(System.out);
        System.out.println();
        
        return soapConnection.call(
        		soapRequest, 
        		url);
	}
	
	public SOAPMessage callInquiryFeeDetailWithSoap(String url, String nsUri, GetItemAmt requestBody) throws Exception {
		// Create SOAP Connection
        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection soapConnection = soapConnectionFactory.createConnection();

        // Send SOAP Message to SOAP Server
        SOAPMessage soapRequest = SoapUtil.createSOAPRequest(url, nsUri, requestBody);
        soapRequest.writeTo(System.out);
        System.out.println();
        
        return soapConnection.call(
        		soapRequest, 
        		url);
	}
	
	public SOAPMessage callInquiryFeeStatusWithSoap(String url, String nsUri, GetPatPayStatus requestBody) throws Exception {
		// Create SOAP Connection
        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection soapConnection = soapConnectionFactory.createConnection();

        // Send SOAP Message to SOAP Server
        SOAPMessage soapRequest = SoapUtil.createSOAPRequest(url, nsUri, requestBody);
        soapRequest.writeTo(System.out);
        System.out.println();
        
        return soapConnection.call(
        		soapRequest, 
        		url);
	}

}
