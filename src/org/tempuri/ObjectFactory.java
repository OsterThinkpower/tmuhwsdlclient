//
// 此檔案是由 JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 所產生 
// 請參閱 <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// 一旦重新編譯來源綱要, 對此檔案所做的任何修改都將會遺失. 
// 產生時間: 2016.05.06 於 11:05:03 AM CST 
//


package org.tempuri;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tempuri package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetItemAmtResponse }
     * 
     */
    public GetItemAmtResponse createGetItemAmtResponse() {
        return new GetItemAmtResponse();
    }

    /**
     * Create an instance of {@link GetSEQNOResponse }
     * 
     */
    public GetSEQNOResponse createGetSEQNOResponse() {
        return new GetSEQNOResponse();
    }

    /**
     * Create an instance of {@link GetAutoamtBasicAPPResponse }
     * 
     */
    public GetAutoamtBasicAPPResponse createGetAutoamtBasicAPPResponse() {
        return new GetAutoamtBasicAPPResponse();
    }

    /**
     * Create an instance of {@link GetAutoamtBasicResponse }
     * 
     */
    public GetAutoamtBasicResponse createGetAutoamtBasicResponse() {
        return new GetAutoamtBasicResponse();
    }

    /**
     * Create an instance of {@link GetPatPayStatusResponse }
     * 
     */
    public GetPatPayStatusResponse createGetPatPayStatusResponse() {
        return new GetPatPayStatusResponse();
    }

    /**
     * Create an instance of {@link GetItemAmtResponse.GetItemAmtResult }
     * 
     */
    public GetItemAmtResponse.GetItemAmtResult createGetItemAmtResponseGetItemAmtResult() {
        return new GetItemAmtResponse.GetItemAmtResult();
    }

    /**
     * Create an instance of {@link SendSMSResponse }
     * 
     */
    public SendSMSResponse createSendSMSResponse() {
        return new SendSMSResponse();
    }

    /**
     * Create an instance of {@link GetAutoamtBasicAPP }
     * 
     */
    public GetAutoamtBasicAPP createGetAutoamtBasicAPP() {
        return new GetAutoamtBasicAPP();
    }

    /**
     * Create an instance of {@link GetSEQNOResponse.GetSEQNOResult }
     * 
     */
    public GetSEQNOResponse.GetSEQNOResult createGetSEQNOResponseGetSEQNOResult() {
        return new GetSEQNOResponse.GetSEQNOResult();
    }

    /**
     * Create an instance of {@link GetSEQNO }
     * 
     */
    public GetSEQNO createGetSEQNO() {
        return new GetSEQNO();
    }

    /**
     * Create an instance of {@link GetItemAmt }
     * 
     */
    public GetItemAmt createGetItemAmt() {
        return new GetItemAmt();
    }

    /**
     * Create an instance of {@link GetAutoamtBasicAPPResponse.GetAutoamtBasicAPPResult }
     * 
     */
    public GetAutoamtBasicAPPResponse.GetAutoamtBasicAPPResult createGetAutoamtBasicAPPResponseGetAutoamtBasicAPPResult() {
        return new GetAutoamtBasicAPPResponse.GetAutoamtBasicAPPResult();
    }

    /**
     * Create an instance of {@link SendSMS }
     * 
     */
    public SendSMS createSendSMS() {
        return new SendSMS();
    }

    /**
     * Create an instance of {@link GetPatPayStatus }
     * 
     */
    public GetPatPayStatus createGetPatPayStatus() {
        return new GetPatPayStatus();
    }

    /**
     * Create an instance of {@link GetAutoamtBasicResponse.GetAutoamtBasicResult }
     * 
     */
    public GetAutoamtBasicResponse.GetAutoamtBasicResult createGetAutoamtBasicResponseGetAutoamtBasicResult() {
        return new GetAutoamtBasicResponse.GetAutoamtBasicResult();
    }

    /**
     * Create an instance of {@link GetAutoamtBasic }
     * 
     */
    public GetAutoamtBasic createGetAutoamtBasic() {
        return new GetAutoamtBasic();
    }

    /**
     * Create an instance of {@link GetPatPayStatusResponse.GetPatPayStatusResult }
     * 
     */
    public GetPatPayStatusResponse.GetPatPayStatusResult createGetPatPayStatusResponseGetPatPayStatusResult() {
        return new GetPatPayStatusResponse.GetPatPayStatusResult();
    }

}
