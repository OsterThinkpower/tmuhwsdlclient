//
// 此檔案是由 JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 所產生 
// 請參閱 <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// 一旦重新編譯來源綱要, 對此檔案所做的任何修改都將會遺失. 
// 產生時間: 2016.05.06 於 11:05:03 AM CST 
//


package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ARG_FEE_NO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ARG_PAY_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ARG_INS_FLAG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ARG_REAL_AMT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ARG_MACHINE_NO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ARG_FEE_DT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ARG_BANK_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ARG_BANK_NO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "argfeeno",
    "argpaytype",
    "arginsflag",
    "argrealamt",
    "argmachineno",
    "argfeedt",
    "argbanktype",
    "argbankno"
})
@XmlRootElement(name = "Get_item_amt")
public class GetItemAmt {

    @XmlElement(name = "ARG_FEE_NO")
    protected String argfeeno;
    @XmlElement(name = "ARG_PAY_TYPE")
    protected String argpaytype;
    @XmlElement(name = "ARG_INS_FLAG")
    protected String arginsflag;
    @XmlElement(name = "ARG_REAL_AMT")
    protected String argrealamt;
    @XmlElement(name = "ARG_MACHINE_NO")
    protected String argmachineno;
    @XmlElement(name = "ARG_FEE_DT")
    protected String argfeedt;
    @XmlElement(name = "ARG_BANK_TYPE")
    protected String argbanktype;
    @XmlElement(name = "ARG_BANK_NO")
    protected String argbankno;

    /**
     * 取得 argfeeno 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getARGFEENO() {
        return argfeeno;
    }

    /**
     * 設定 argfeeno 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setARGFEENO(String value) {
        this.argfeeno = value;
    }

    /**
     * 取得 argpaytype 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getARGPAYTYPE() {
        return argpaytype;
    }

    /**
     * 設定 argpaytype 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setARGPAYTYPE(String value) {
        this.argpaytype = value;
    }

    /**
     * 取得 arginsflag 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getARGINSFLAG() {
        return arginsflag;
    }

    /**
     * 設定 arginsflag 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setARGINSFLAG(String value) {
        this.arginsflag = value;
    }

    /**
     * 取得 argrealamt 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getARGREALAMT() {
        return argrealamt;
    }

    /**
     * 設定 argrealamt 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setARGREALAMT(String value) {
        this.argrealamt = value;
    }

    /**
     * 取得 argmachineno 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getARGMACHINENO() {
        return argmachineno;
    }

    /**
     * 設定 argmachineno 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setARGMACHINENO(String value) {
        this.argmachineno = value;
    }

    /**
     * 取得 argfeedt 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getARGFEEDT() {
        return argfeedt;
    }

    /**
     * 設定 argfeedt 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setARGFEEDT(String value) {
        this.argfeedt = value;
    }

    /**
     * 取得 argbanktype 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getARGBANKTYPE() {
        return argbanktype;
    }

    /**
     * 設定 argbanktype 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setARGBANKTYPE(String value) {
        this.argbanktype = value;
    }

    /**
     * 取得 argbankno 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getARGBANKNO() {
        return argbankno;
    }

    /**
     * 設定 argbankno 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setARGBANKNO(String value) {
        this.argbankno = value;
    }

}
