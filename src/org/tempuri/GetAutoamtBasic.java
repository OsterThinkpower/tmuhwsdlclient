//
// 此檔案是由 JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 所產生 
// 請參閱 <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// 一旦重新編譯來源綱要, 對此檔案所做的任何修改都將會遺失. 
// 產生時間: 2016.05.06 於 11:05:03 AM CST 
//


package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ARG_CHR_NO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ARG_ID_NO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ARG_BIRTH_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ARG_FEE_NO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "argchrno",
    "argidno",
    "argbirthdate",
    "argfeeno"
})
@XmlRootElement(name = "Get_autoamt_basic")
public class GetAutoamtBasic {

    @XmlElement(name = "ARG_CHR_NO")
    protected String argchrno;
    @XmlElement(name = "ARG_ID_NO")
    protected String argidno;
    @XmlElement(name = "ARG_BIRTH_DATE")
    protected String argbirthdate;
    @XmlElement(name = "ARG_FEE_NO")
    protected String argfeeno;

    /**
     * 取得 argchrno 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getARGCHRNO() {
        return argchrno;
    }

    /**
     * 設定 argchrno 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setARGCHRNO(String value) {
        this.argchrno = value;
    }

    /**
     * 取得 argidno 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getARGIDNO() {
        return argidno;
    }

    /**
     * 設定 argidno 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setARGIDNO(String value) {
        this.argidno = value;
    }

    /**
     * 取得 argbirthdate 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getARGBIRTHDATE() {
        return argbirthdate;
    }

    /**
     * 設定 argbirthdate 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setARGBIRTHDATE(String value) {
        this.argbirthdate = value;
    }

    /**
     * 取得 argfeeno 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getARGFEENO() {
        return argfeeno;
    }

    /**
     * 設定 argfeeno 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setARGFEENO(String value) {
        this.argfeeno = value;
    }

}
